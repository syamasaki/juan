<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Shop $shop
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Shops'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Shop Reviews'), ['controller' => 'ShopReviews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Shop Review'), ['controller' => 'ShopReviews', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List User Reviews'), ['controller' => 'UserReviews', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User Review'), ['controller' => 'UserReviews', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="shops form large-9 medium-8 columns content">
    <?= $this->Form->create($shop) ?>
    <fieldset>
        <legend><?= __('Add Shop') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
