<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Shop $shop
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Shop'), ['action' => 'edit', $shop->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Shop'), ['action' => 'delete', $shop->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shop->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Shops'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Shop'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Shop Reviews'), ['controller' => 'ShopReviews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Shop Review'), ['controller' => 'ShopReviews', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Reviews'), ['controller' => 'UserReviews', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Review'), ['controller' => 'UserReviews', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="shops view large-9 medium-8 columns content">
    <h3><?= h($shop->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($shop->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($shop->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($shop->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($shop->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Shop Reviews') ?></h4>
        <?php if (!empty($shop->shop_reviews)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Shop Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Score') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($shop->shop_reviews as $shopReviews): ?>
            <tr>
                <td><?= h($shopReviews->id) ?></td>
                <td><?= h($shopReviews->shop_id) ?></td>
                <td><?= h($shopReviews->user_id) ?></td>
                <td><?= h($shopReviews->score) ?></td>
                <td><?= h($shopReviews->created) ?></td>
                <td><?= h($shopReviews->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ShopReviews', 'action' => 'view', $shopReviews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ShopReviews', 'action' => 'edit', $shopReviews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ShopReviews', 'action' => 'delete', $shopReviews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shopReviews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Reviews') ?></h4>
        <?php if (!empty($shop->user_reviews)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Shop Id') ?></th>
                <th scope="col"><?= __('Score') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($shop->user_reviews as $userReviews): ?>
            <tr>
                <td><?= h($userReviews->id) ?></td>
                <td><?= h($userReviews->user_id) ?></td>
                <td><?= h($userReviews->shop_id) ?></td>
                <td><?= h($userReviews->score) ?></td>
                <td><?= h($userReviews->created) ?></td>
                <td><?= h($userReviews->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserReviews', 'action' => 'view', $userReviews->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserReviews', 'action' => 'edit', $userReviews->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserReviews', 'action' => 'delete', $userReviews->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userReviews->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
