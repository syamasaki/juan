<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserReview $userReview
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User Review'), ['action' => 'edit', $userReview->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User Review'), ['action' => 'delete', $userReview->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userReview->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User Reviews'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Review'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="userReviews view large-9 medium-8 columns content">
    <h3><?= h($userReview->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $userReview->has('user') ? $this->Html->link($userReview->user->name, ['controller' => 'Users', 'action' => 'view', $userReview->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Shop') ?></th>
            <td><?= $userReview->has('shop') ? $this->Html->link($userReview->shop->name, ['controller' => 'Shops', 'action' => 'view', $userReview->shop->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($userReview->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($userReview->score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($userReview->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($userReview->modified) ?></td>
        </tr>
    </table>
</div>
