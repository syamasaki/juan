<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UserReview[]|\Cake\Collection\CollectionInterface $userReviews
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New User Review'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="userReviews index large-9 medium-8 columns content">
    <h3><?= __('User Reviews') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('shop_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('score') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($userReviews as $userReview): ?>
            <tr>
                <td><?= $this->Number->format($userReview->id) ?></td>
                <td><?= $userReview->has('user') ? $this->Html->link($userReview->user->name, ['controller' => 'Users', 'action' => 'view', $userReview->user->id]) : '' ?></td>
                <td><?= $userReview->has('shop') ? $this->Html->link($userReview->shop->name, ['controller' => 'Shops', 'action' => 'view', $userReview->shop->id]) : '' ?></td>
                <td><?= $this->Number->format($userReview->score) ?></td>
                <td><?= h($userReview->created) ?></td>
                <td><?= h($userReview->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $userReview->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $userReview->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $userReview->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userReview->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
