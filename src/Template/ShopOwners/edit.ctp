<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ShopOwner $shopOwner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $shopOwner->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $shopOwner->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Shop Owners'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="shopOwners form large-9 medium-8 columns content">
    <?= $this->Form->create($shopOwner) ?>
    <fieldset>
        <legend><?= __('Edit Shop Owner') ?></legend>
        <?php
            echo $this->Form->control('email');
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
