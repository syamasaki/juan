<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ShopOwner $shopOwner
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Shop Owners'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="shopOwners form large-9 medium-8 columns content">
    <?= $this->Form->create($shopOwner) ?>
    <fieldset>
        <legend><?= __('Add Shop Owner') ?></legend>
        <?php
            echo $this->Form->control('email');
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
