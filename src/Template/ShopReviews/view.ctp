<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ShopReview $shopReview
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Shop Review'), ['action' => 'edit', $shopReview->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Shop Review'), ['action' => 'delete', $shopReview->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shopReview->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Shop Reviews'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Shop Review'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="shopReviews view large-9 medium-8 columns content">
    <h3><?= h($shopReview->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Shop') ?></th>
            <td><?= $shopReview->has('shop') ? $this->Html->link($shopReview->shop->name, ['controller' => 'Shops', 'action' => 'view', $shopReview->shop->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $shopReview->has('user') ? $this->Html->link($shopReview->user->name, ['controller' => 'Users', 'action' => 'view', $shopReview->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($shopReview->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Score') ?></th>
            <td><?= $this->Number->format($shopReview->score) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($shopReview->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($shopReview->modified) ?></td>
        </tr>
    </table>
</div>
