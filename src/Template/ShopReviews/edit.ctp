<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ShopReview $shopReview
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $shopReview->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $shopReview->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Shop Reviews'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="shopReviews form large-9 medium-8 columns content">
    <?= $this->Form->create($shopReview) ?>
    <fieldset>
        <legend><?= __('Edit Shop Review') ?></legend>
        <?php
            echo $this->Form->control('shop_id', ['options' => $shops]);
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('score');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
