<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ShopReview[]|\Cake\Collection\CollectionInterface $shopReviews
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Shop Review'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Shops'), ['controller' => 'Shops', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Shop'), ['controller' => 'Shops', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="shopReviews index large-9 medium-8 columns content">
    <h3><?= __('Shop Reviews') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('shop_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('score') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($shopReviews as $shopReview): ?>
            <tr>
                <td><?= $this->Number->format($shopReview->id) ?></td>
                <td><?= $shopReview->has('shop') ? $this->Html->link($shopReview->shop->name, ['controller' => 'Shops', 'action' => 'view', $shopReview->shop->id]) : '' ?></td>
                <td><?= $shopReview->has('user') ? $this->Html->link($shopReview->user->name, ['controller' => 'Users', 'action' => 'view', $shopReview->user->id]) : '' ?></td>
                <td><?= $this->Number->format($shopReview->score) ?></td>
                <td><?= h($shopReview->created) ?></td>
                <td><?= h($shopReview->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $shopReview->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $shopReview->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $shopReview->id], ['confirm' => __('Are you sure you want to delete # {0}?', $shopReview->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
