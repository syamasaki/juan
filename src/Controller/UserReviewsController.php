<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UserReviews Controller
 *
 * @property \App\Model\Table\UserReviewsTable $UserReviews
 *
 * @method \App\Model\Entity\UserReview[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UserReviewsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Shops']
        ];
        $userReviews = $this->paginate($this->UserReviews);

        $this->set(compact('userReviews'));
    }

    /**
     * View method
     *
     * @param string|null $id User Review id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $userReview = $this->UserReviews->get($id, [
            'contain' => ['Users', 'Shops']
        ]);

        $this->set('userReview', $userReview);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $userReview = $this->UserReviews->newEntity();
        if ($this->request->is('post')) {
            $userReview = $this->UserReviews->patchEntity($userReview, $this->request->getData());
            if ($this->UserReviews->save($userReview)) {
                $this->Flash->success(__('The user review has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user review could not be saved. Please, try again.'));
        }
        $users = $this->UserReviews->Users->find('list', ['limit' => 200]);
        $shops = $this->UserReviews->Shops->find('list', ['limit' => 200]);
        $this->set(compact('userReview', 'users', 'shops'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User Review id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $userReview = $this->UserReviews->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $userReview = $this->UserReviews->patchEntity($userReview, $this->request->getData());
            if ($this->UserReviews->save($userReview)) {
                $this->Flash->success(__('The user review has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user review could not be saved. Please, try again.'));
        }
        $users = $this->UserReviews->Users->find('list', ['limit' => 200]);
        $shops = $this->UserReviews->Shops->find('list', ['limit' => 200]);
        $this->set(compact('userReview', 'users', 'shops'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User Review id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $userReview = $this->UserReviews->get($id);
        if ($this->UserReviews->delete($userReview)) {
            $this->Flash->success(__('The user review has been deleted.'));
        } else {
            $this->Flash->error(__('The user review could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
