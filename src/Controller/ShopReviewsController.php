<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ShopReviews Controller
 *
 * @property \App\Model\Table\ShopReviewsTable $ShopReviews
 *
 * @method \App\Model\Entity\ShopReview[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShopReviewsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Shops', 'Users']
        ];
        $shopReviews = $this->paginate($this->ShopReviews);

        $this->set(compact('shopReviews'));
    }

    /**
     * View method
     *
     * @param string|null $id Shop Review id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $shopReview = $this->ShopReviews->get($id, [
            'contain' => ['Shops', 'Users']
        ]);

        $this->set('shopReview', $shopReview);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $shopReview = $this->ShopReviews->newEntity();
        if ($this->request->is('post')) {
            $shopReview = $this->ShopReviews->patchEntity($shopReview, $this->request->getData());
            if ($this->ShopReviews->save($shopReview)) {
                $this->Flash->success(__('The shop review has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The shop review could not be saved. Please, try again.'));
        }
        $shops = $this->ShopReviews->Shops->find('list', ['limit' => 200]);
        $users = $this->ShopReviews->Users->find('list', ['limit' => 200]);
        $this->set(compact('shopReview', 'shops', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Shop Review id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $shopReview = $this->ShopReviews->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $shopReview = $this->ShopReviews->patchEntity($shopReview, $this->request->getData());
            if ($this->ShopReviews->save($shopReview)) {
                $this->Flash->success(__('The shop review has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The shop review could not be saved. Please, try again.'));
        }
        $shops = $this->ShopReviews->Shops->find('list', ['limit' => 200]);
        $users = $this->ShopReviews->Users->find('list', ['limit' => 200]);
        $this->set(compact('shopReview', 'shops', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Shop Review id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $shopReview = $this->ShopReviews->get($id);
        if ($this->ShopReviews->delete($shopReview)) {
            $this->Flash->success(__('The shop review has been deleted.'));
        } else {
            $this->Flash->error(__('The shop review could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
