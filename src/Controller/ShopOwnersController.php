<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ShopOwners Controller
 *
 * @property \App\Model\Table\ShopOwnersTable $ShopOwners
 *
 * @method \App\Model\Entity\ShopOwner[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShopOwnersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $shopOwners = $this->paginate($this->ShopOwners);

        $this->set(compact('shopOwners'));
    }

    /**
     * View method
     *
     * @param string|null $id Shop Owner id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $shopOwner = $this->ShopOwners->get($id, [
            'contain' => []
        ]);

        $this->set('shopOwner', $shopOwner);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $shopOwner = $this->ShopOwners->newEntity();
        if ($this->request->is('post')) {
            $shopOwner = $this->ShopOwners->patchEntity($shopOwner, $this->request->getData());
            if ($this->ShopOwners->save($shopOwner)) {
                $this->Flash->success(__('The shop owner has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The shop owner could not be saved. Please, try again.'));
        }
        $this->set(compact('shopOwner'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Shop Owner id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $shopOwner = $this->ShopOwners->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $shopOwner = $this->ShopOwners->patchEntity($shopOwner, $this->request->getData());
            if ($this->ShopOwners->save($shopOwner)) {
                $this->Flash->success(__('The shop owner has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The shop owner could not be saved. Please, try again.'));
        }
        $this->set(compact('shopOwner'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Shop Owner id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $shopOwner = $this->ShopOwners->get($id);
        if ($this->ShopOwners->delete($shopOwner)) {
            $this->Flash->success(__('The shop owner has been deleted.'));
        } else {
            $this->Flash->error(__('The shop owner could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
