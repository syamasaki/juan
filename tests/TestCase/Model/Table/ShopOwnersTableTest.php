<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ShopOwnersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ShopOwnersTable Test Case
 */
class ShopOwnersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ShopOwnersTable
     */
    public $ShopOwners;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.shop_owners'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ShopOwners') ? [] : ['className' => ShopOwnersTable::class];
        $this->ShopOwners = TableRegistry::getTableLocator()->get('ShopOwners', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ShopOwners);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
