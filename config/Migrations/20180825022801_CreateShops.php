<?php
use Migrations\AbstractMigration;

class CreateShops extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('shops');

        $table->addColumn("name", "string", [
            "default" => "",
            "limit" => 255,
            "null" => false
        ]);
        $table->addColumn("created", "datetime", [
            "default" => null,
            "null" => true
        ]);
        $table->addColumn("modified", "datetime", [
            "default" => null,
            "null" => true
        ]);

        $table->create();
    }
}
