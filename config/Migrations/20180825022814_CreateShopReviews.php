<?php
use Migrations\AbstractMigration;

class CreateShopReviews extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('shop_reviews');

        $table->addColumn("shop_id", "integer", [
            "default" => 0,
            "null" => false
        ]);
        $table->addColumn("user_id", "integer", [
            "default" => 0,
            "null" => false
        ]);
        $table->addColumn("score", "integer", [
            "default" => 0,
            "null" => false
        ]);
        $table->addColumn("created", "datetime", [
            "default" => null,
            "null" => true
        ]);
        $table->addColumn("modified", "datetime", [
            "default" => null,
            "null" => true
        ]);

        $table->addIndex(["shop_id"] , ["name" => "idx_shop_id"]);
        $table->addIndex(["user_id"] , ["name" => "idx_user_id"]);
        $table->create();
    }
}
