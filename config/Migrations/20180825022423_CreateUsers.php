<?php
use Migrations\AbstractMigration;

class CreateUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');

        $table->addColumn("email", "string", [
            "default" => "",
            "limit" => 255,
            "null" => false
        ]);
        $table->addColumn("name", "string", [
            "default" => "",
            "limit" => 255,
            "null" => false
        ]);
        $table->addColumn("created", "datetime", [
            "default" => null,
            "null" => true
        ]);
        $table->addColumn("modified", "datetime", [
            "default" => null,
            "null" => true
        ]);

        $table->addIndex(["email"] , ["name" => "idx_email", "unique"=>true]);
        $table->create();
    }
}
